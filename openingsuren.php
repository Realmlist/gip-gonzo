<h2 style="color:#333;">Openingsuren</h2>
<?php
$datum = date("w");
$dagen = array("zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag");
$dag = $dagen[$datum];

if ($dag == "zondag"){
	echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"235px\">
  <tr>
    <td width=\"99\">Maandag</td>
    <td width=\"273\"><div align=\"right\">Gesloten</div></td>
  </tr>
  <tr>
    <td>Dinsdag</td>
    <td><div align=\"right\">19u - ...</div></td>
  </tr>
  <tr>
    <td>Woensdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td>Donderdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td>Vrijdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td>Zaterdag</td>
    <td><div align=\"right\">19u - ...</div></td>
  </tr>
  <tr>
    <td><strong>Zondag</strong></td>
    <td><div align=\"right\"><strong>Gesloten</strong></div></td>
  </tr>
</table>";
}elseif ($dag == "maandag"){
	echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"235px\">
  <tr>
    <td width=\"99\"><strong>Maandag</strong></td>
    <td width=\"273\"><div align=\"right\"><strong>Gesloten</strong></div></td>
  </tr>
  <tr>
    <td>Dinsdag</td>
    <td><div align=\"right\">19u - ...</div></td>
  </tr>
  <tr>
    <td>Woensdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td>Donderdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td>Vrijdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td>Zaterdag</td>
    <td><div align=\"right\">19u - ...</div></td>
  </tr>
  <tr>
    <td>Zondag</td>
    <td><div align=\"right\">Gesloten</div></td>
  </tr>
</table>";
}elseif ($dag == "dinsdag"){
	echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"235px\">
  <tr>
    <td width=\"99\">Maandag</td>
    <td width=\"273\"><div align=\"right\">Gesloten</div></td>
  </tr>
  <tr>
    <td><strong>Dinsdag</strong></td>
    <td><div align=\"right\"><strong>19u - ...</strong></div></td>
  </tr>
  <tr>
    <td>Woensdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td>Donderdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td>Vrijdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td>Zaterdag</td>
    <td><div align=\"right\">19u - ...</div></td>
  </tr>
  <tr>
    <td>Zondag</td>
    <td><div align=\"right\">Gesloten</div></td>
  </tr>
</table>";
}elseif ($dag == "woensdag"){
	echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"235px\">
  <tr>
    <td width=\"99\">Maandag</td>
    <td width=\"273\"><div align=\"right\">Gesloten</div></td>
  </tr>
  <tr>
    <td>Dinsdag</td>
    <td><div align=\"right\">19u - ...</div></td>
  </tr>
  <tr>
    <td><strong>Woensdag</strong></td>
    <td><div align=\"right\"><strong>16u - ...</strong></div></td>
  </tr>
  <tr>
    <td>Donderdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td>Vrijdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td>Zaterdag</td>
    <td><div align=\"right\">19u - ...</div></td>
  </tr>
  <tr>
    <td>Zondag</td>
    <td><div align=\"right\">Gesloten</div></td>
  </tr>
</table>";
}elseif ($dag == "donderdag"){
	echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"235px\">
  <tr>
    <td width=\"99\">Maandag</td>
    <td width=\"273\"><div align=\"right\">Gesloten</div></td>
  </tr>
  <tr>
    <td>Dinsdag</td>
    <td><div align=\"right\">19u - ...</div></td>
  </tr>
  <tr>
    <td>Woensdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td><strong>Donderdag</strong></td>
    <td><div align=\"right\"><strong>16u - ...</strong></div></td>
  </tr>
  <tr>
    <td>Vrijdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td>Zaterdag</td>
    <td><div align=\"right\">19u - ...</div></td>
  </tr>
  <tr>
    <td>Zondag</td>
    <td><div align=\"right\">Gesloten</div></td>
  </tr>
</table>";
}elseif ($dag == "vrijdag"){
	echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"235px\">
  <tr>
    <td width=\"99\">Maandag</td>
    <td width=\"273\"><div align=\"right\">Gesloten</div></td>
  </tr>
  <tr>
    <td>Dinsdag</td>
    <td><div align=\"right\">19u - ...</div></td>
  </tr>
  <tr>
    <td>Woensdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td>Donderdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td><strong>Vrijdag</strong></td>
    <td><div align=\"right\"><strong>16u - ...</strong></div></td>
  </tr>
  <tr>
    <td>Zaterdag</td>
    <td><div align=\"right\">19u - ...</div></td>
  </tr>
  <tr>
    <td>Zondag</td>
    <td><div align=\"right\">Gesloten</div></td>
  </tr>
</table>";
}elseif ($dag == "zaterdag"){
	echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"235px\">
  <tr>
    <td width=\"99\">Maandag</td>
    <td width=\"273\"><div align=\"right\">Gesloten</div></td>
  </tr>
  <tr>
    <td>Dinsdag</td>
    <td><div align=\"right\">19u - ...</div></td>
  </tr>
  <tr>
    <td>Woensdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td>Donderdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td>Vrijdag</td>
    <td><div align=\"right\">16u - ...</div></td>
  </tr>
  <tr>
    <td><strong>Zaterdag</strong></td>
    <td><div align=\"right\"><strong>19u - ...</strong></div></td>
  </tr>
  <tr>
    <td>Zondag</td>
    <td><div align=\"right\">Gesloten</div></td>
  </tr>
</table>";
}else{
	echo "Fout.";
}


?>
<hr noshade="noshade" size="1" color="#232323" />