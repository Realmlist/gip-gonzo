<style type="text/css">
form, td{
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
}
input[type=text], input[type=file], textarea{
	width:370px;
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
}
.error{
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	color:#F00;
	font-weight:bold;
	text-decoration:overline underline;
}
</style><?php
echo "<div align=\"center\" style=\"border:medium #999 solid;\">";
extract($_POST);
if(isset($submit)){
	if (empty($naamb)){
		echo "<div class=\"error\"><p>Gelieve de naam van uw band in te vullen!</p></div>";
	}elseif (empty($genre)){
		echo "<div class=\"error\"><p>Gelieve het genre van uw groep in te vullen!</p></div>";
	}elseif (empty($naamv)){
		echo "<div class=\"error\"><p>Gelieve uw naam in te vullen!</p></div>";
	}elseif (empty($emailv)){
		echo "<div class=\"error\"><p>Gelieve uw email in te vullen!</p></div>";
	}elseif (empty($datum)){
		echo "<div class=\"error\"><p>Gelieve uw gewenste speeldatum in te vullen!</p></div>";
	}elseif (empty($site)){
		echo "<div class=\"error\"><p>Gelieve uw website in te vullen! Indien u er geen heeft 
				zet u dan \"Geen website\" in het tekstveld.</p></div>";
	}else{ // Alles oké? -> filter inputs
	$naamb = htmlspecialchars($naamb, ENT_QUOTES);
	$naamv = htmlspecialchars($naamv, ENT_QUOTES);
	$genre = htmlspecialchars($genre, ENT_QUOTES);
	// En inserten in DB.
		mysql_query("INSERT INTO aanvraag VALUES ('', '" . mysql_real_escape_string($naamb) . "', '" . mysql_real_escape_string($site) . "', '" . mysql_real_escape_string($naamv) . "', '" . mysql_real_escape_string($emailv) . "', 'Toegevoegd door " . $loginnaam . " . (Toevoegen-pagina.)', '" . mysql_real_escape_string($datuma) . "', '" . mysql_real_escape_string($genre) . "', '2')");
		echo "<div class=\"succes\">Bedankt $naamv, uw band is toegevoegd!<br /></div>";
		die;
	}
}
?>
<form name="formulier" action="?p=admin&submenu=aanvragen&actie=toevoegen" method="post">
  <table width="100%" border="0" cellspacing="5" cellpadding="5">
    <tr>
      <td>Naam band:</td>
      <td><label>
        <input type="text" name="naamb" id="textfield" value="<?php if (isset($naamb)) {echo"$naamb"; } ?>">
      </label></td>
    </tr>
    <tr>
      <td>Datum:</td>
      <td><label>
        <input name="datuma" type="text" id="textfield2" readonly="readonly" value="<?php if (isset($datuma)) {echo"$datuma"; } ?>">
        <a href="#"
   onClick="cal.select(document.forms['formulier'].datuma,'anchor1','dd/MM/yyyy'); return false;" name="anchor1" id="anchor1">Selecteren</A>
<div id="calender" style="position:absolute;visibility:hidden;color:#000;background-color:white;layer-background-color:white;"></div>
      </label></td>
    </tr>
    <tr>
      <td>Genre:</td>
      <td><label>
        <input type="text" name="genre" id="textfield3" value="<?php if (isset($genre)) {echo"$genre"; } ?>">
      </label></td>
    </tr>
    <tr>
      <td>Website:</td>
      <td><label>
        <input type="text" name="site" id="textfield4" value="<?php if (isset($site)) {echo"$site"; } ?>">
      </label></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Naam verantwoordelijke:</td>
      <td><label>
        <input type="text" name="naamv" id="textfield5" value="<?php if (isset($naamv)) {echo"$naamv"; } ?>">
      </label></td>
    </tr>
    <tr>
      <td>E-mail verantwoordelijke:</td>
      <td><label>
        <input type="text" name="emailv" id="textfield6" value="<?php if (isset($emailv)) {echo"$emailv"; } ?>">
      </label></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><label>
        <input type="submit" name="submit" id="button" value="Verzenden">
      </label></td>
    </tr>
  </table>
</form>
</div>