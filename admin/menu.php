<?php 
	////////////Aanvraagteller/////////////
	
	$querystate = "SELECT COUNT(state) FROM aanvraag WHERE state = 0"; 
	$result = mysql_query($querystate) or die(mysql_error());
	$recordaanvraag = mysql_fetch_array($result);
	
	$count = $recordaanvraag['COUNT(state)'];
	
	////////////Rechtenbeheer//////////////
	
	$user = $_SESSION['user'];
	$pw = $_SESSION['pasw'];
	
	$queryrechten = "SELECT * FROM login WHERE login = '$user' AND paswoord = '$pw'";
	
	$resultrechten = mysql_query($queryrechten);
		
	$record = mysql_fetch_object($resultrechten);
	
	// Records in vars steken.
	$drank = "$record->drankbeheren";
	$fotos = "$record->fotosbeheren";
	$aanvragen = "$record->aanvragenbeheren";
	$agenda = "$record->agendabeheren";
	$homepage = "$record->homepagebeheren";
	$loginnaam = "$record->voornaam";
	
	// Demo beveiliging
	if($loginnaam == "demo")	{
		$homelink = "#";
		$dranklinkEdit = "#";
		$dranklinkAdd = "#";
		$dranklinkDel = "#";
		$fotolinkAdd = "#";
		$fotolinkEdit = "#";
		$fotolinkDel = "#";
		$agendalinkEdit = "#";
		$agendalinkAdd = "#";
		$agendalinkDel = "#";
		$aanvraaglinkAdd = "#";
		$aanvraaglink = "#";
	}else{
		$homelink = "?p=admin&submenu=homepage&actie=aanpassen";
		$dranklinkEdit = "?p=admin&submenu=drank&actie=aanpassen";
		$dranklinkAdd = "?p=admin&submenu=drank&actie=toevoegen";
		$dranklinkDel = "?p=admin&submenu=drank&actie=verwijderen";
		$fotolinkAdd = "?p=admin&submenu=foto&actie=toevoegen";
		$fotolinkEdit = "?p=admin&submenu=foto&actie=aanpassen";
		$fotolinkDel = "?p=admin&submenu=foto&actie=verwijderen";
		$agendalinkEdit = "?p=admin&submenu=agenda&actie=aanpassen";
		$agendalinkAdd = "?p=admin&submenu=agenda&actie=toevoegen";
		$agendalinkDel = "?p=admin&submenu=agenda&actie=verwijderen";
		$aanvraaglinkAdd = "?p=admin&submenu=aanvragen&actie=toevoegen";
		$aanvraaglink = "?p=admin&submenu=aanvragen&actie=beheer";
	}
?>
<div id="admin-bar">
		<ul>
			<li class="welcome">Welkom, <?php echo $loginnaam ?>!</li>
		</ul>
        <?php 
		if ($homepage == "ja"){
			echo "<ul class=\"controls\">
				<li class=\"icon\"><a style=\"cursor:default\" href=\"" . $homelink . "\" data-tip=\"Homepagina aanpassen\">S</a></li>
			</ul>";
		}else{
			echo "";
		}
		?>
		<ul class="controls">
         <?php 
			if ($drank == "ja"){
				echo "<li class=\"more\"><a href=\"#\" style=\"cursor:default\">Drank</a>
					<ul class=\"menu-item\">
						<li><a style=\"cursor:default\" href=\"". $dranklinkEdit ."\">Aanpassen</a></li>
						<li><a style=\"cursor:default\" href=\"". $dranklinkAdd ."\">Toevoegen</a></li>
						<li><a style=\"cursor:default\" href=\"". $dranklinkDel ."\">Verwijderen</a></li>
					</ul>
				</li>";
			}else{
				echo "";
			} 
			if ($fotos == "ja"){
				echo "<li class=\"more\"><a href=\"#\" style=\"cursor:default\">Foto's</a>
					<ul class=\"menu-item\">
						<li><a style=\"cursor:default\" href=\"". $fotolinkEdit ."\">Aanpassen</a></li>
						<li><a style=\"cursor:default\" href=\"". $fotolinkAdd ."\">Toevoegen</a></li>
						<li><a style=\"cursor:default\" href=\"". $fotolinkDel ."\">Verwijderen</a></li>
					</ul>
				</li>";
				}else{
					echo "";
				}
				 
			if ($agenda == "ja"){
				echo "<li class=\"more\"><a href=\"#\" style=\"cursor:default\">Agenda</a>
					<ul class=\"menu-item\">
						<li><a style=\"cursor:default\" href=\"". $agendalinkEdit ."\">Aanpassen</a></li>
						<li><a style=\"cursor:default\" href=\"". $agendalinkAdd ."\">Toevoegen</a></li>
						<li><a style=\"cursor:default\" href=\"". $agendalinkDel ."\">Verwijderen</a></li>
					</ul>
				</li>";
			}else{
				echo "";
			} 
			if ($aanvragen == "ja"){
				echo "<li class=\"more\"><a href=\"#\" style=\"cursor:default\">Aanvragen<span class=\"priority notice\" title=\"" . $count . " nieuwe aanvragen\">
				" . $count . "</span></a>
				<ul class=\"menu-item\">
				<li><a style=\"cursor:default\" href=\"" .$aanvraaglink ."\">Beheren</a></li>
				<li><a style=\"cursor:default\" href=\"". $aanvraaglinkAdd ."\">Toevoegen</a></li>
				</ul>
				</li>";
			}else{
				echo "";
			}
		?>
        </ul>
		<ul class="controls">
			<li class="icon"><a style="cursor:default" href="?p=admin&submenu=logout" class="priority" data-tip="Uitloggen">X</a></li>
		</ul>
</div>