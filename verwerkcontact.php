<?php


extract($_POST);
// your email address
$myemail = "admin@fabricedesmet.be";

if(isset($button))
{		
	  require_once('recaptchalib.php');  
      $privatekey = "6Le_bcgSAAAAAKcbcbNBheMbuvloWrvhbqGAUiXd";
	  $resp = recaptcha_check_answer ($privatekey,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);

 		 if (!$resp->is_valid) 
		 {
			 // What happens when the CAPTCHA was entered incorrectly
			 echo "<div class=\"error\"><p>De reCAPTCHA was niet correct ingevoerd. Probeer opnieuw.</p></div>";
			 include_once("contact.php");
		} 
		  
		  
		else 
		{ // Als reCAPTCHA valid is -> Alles controleren

				// veld validatie
				if (empty($email) || empty($vraag) || empty($naam) || empty($voornaam))
				
				{
					echo "<div class=\"error\"><p>Alle velden zijn vereist! Probeer opnieuw.</p></div>";
					include ("contact.php");
				}
				
				elseif (empty($phone)) {
					$phone = "Geen telefoonnummer ingevoerd.";
				}	else  {
				
					// email validatie
					if(!eregi('^([._a-z0-9-]+[._a-z0-9-]*)@(([a-z0-9-]+\.)*([a-z0-9-]+)(\.[a-z]{2,3})?)$', $email)) 
					{
						echo "<div class=\"error\"><p>Uw e-mail is niet correct. Probeer opnieuw.</p></div>";
						include ("contact.php");
						
					}
				
					// Headers
					$headers = "From: \"$naam\" <$email>" . "\r\n";
					$headers .= "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
					// Mail zelf
					$subject = "Contact formulier Gonzo";
					$vraag2 = nl2br($vraag);
					$message = "<p><strong>" . $voornaam . " " . $naam ." vroeg het volgende:</strong><br /><br /> ". $vraag2 ."</p><br /><br /><br /> 
								<p><strong>Additionele informatie:</strong><br />
								Telefoonnummer: " . $phone . "<br />
								E-mail: " . $email . "</p>
								";
					// Verzenden van mail
					mail ("$myemail", "$subject", $message, $headers);
					echo "<div class=\"succes\">Bedankt $voornaam, uw e-mail is verzonden!</div>";
				
					
				} // einde else
			}

} // einde submit
?>