<?php
session_start();
include_once('config.php');

if ($_GET['p'] == "admin")						{			$_SESSION['splash'] = "skip" ;}

if ($_SESSION['splash'] == NULL)				{			header('Location: splash.php');}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Muziekcafé Gonzo</title>
<meta http-equiv="keywords" content="Gonzo, café, cafe, Ninove, uitgaan, alternative, muziek, muziekcafé, muziekcafe, oost-vlaanderen, oost vlaanderen, o-vl, ninof, metal, rock, punk, ska, concerten, concert, optreden, optredens" />
<?php include_once('analytics.php'); ?>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
<link rel="stylesheet" type="text/css" href="stijl.css" />
<!--[if IE]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js">
      </script>
<![endif]-->

<!-- http://www.lytebox.com/ -->
<script type="text/javascript" language="javascript" src="lytebox.js"></script>
<link rel="stylesheet" href="lytebox.css" type="text/css" media="screen" />
<style type="text/css">
<?php
	if ($_GET['p'] == "admin" && $_GET['submenu'] == "aanvragen" && $_GET['actie'] == "beheer"){
	echo "section{
	margin-right:auto
	margin-top:2%;
	margin-left:auto;
	font:Verdana, Geneva, sans-serif;
	font-size:12px;
	color:#FFF;
	background-color:#232323;
	
	width:1400px;
	min-height:863px;	
	
	border-radius:50px;
	-moz-border-radius:50px;
	-o-border-radius:50px;
	-webkit-border-radius:50px;
}
	header, .leftbox, #boxhouder{
	display:none;
}
";
	}else{
		echo "section{
	margin-right:2%;
	margin-top:2%;
	margin-left:500px;
	font:Verdana, Geneva, sans-serif;
	font-size:12px;
	color:#FFF;
	background-color:#232323;
	
	width:850px;
	min-height:863px;	
	
	border-radius:50px;
	-moz-border-radius:50px;
	-o-border-radius:50px;
	-webkit-border-radius:50px;
}
";}

?>
/* http://www.fontsquirrel.com/fontface/generator */
@font-face {
    font-family: 'WebSymbolsRegular';
    src: url('/fonts/websymbols-regular-webfont.eot');
    src: url('/fonts/websymbols-regular-webfont.eot?#iefix') format('embedded-opentype'),
         url('/fonts/websymbols-regular-webfont.woff') format('woff'),
         url('/fonts/websymbols-regular-webfont.ttf') format('truetype'),
         url('/fonts/websymbols-regular-webfont.svg#WebSymbolsRegular') format('svg');
	font-weight:normal;
    
}
a:link, a:visited, a:active{
	color:#FFF;
	text-decoration:none;
}
a:hover{
	color:#FFF;
	text-decoration:underline;
}

/*#navigationMenu span a:link, a:visited, a:active{
	color:#999;
	text-decoration:none;
}*/

#navigationMenu span a:hover{
	color:#FFF;
	text-decoration:none;
	border-bottom:#FFF solid 1px;
}
#navigationMenu li{
	display: block;
	list-style:none;
	height:95px;
	padding:2px;
	width:40px;
}

#navigationMenu span{
	width:0px;
	left:0px;
	padding:0px;
	position:absolute;
	overflow:hidden;

	
	font-family:Verdana, Geneva, sans-serif;
	font-size:18px;
	font-weight:bold;
	letter-spacing:0.6px;
	white-space:nowrap;
	line-height:65px;
	
	-webkit-transition: all 0.2s ease-in;
	-moz-transition: all 0.2s ease-in;
	-o-transition: all 0.2s ease-in;
	-ms-transition: all 0.2s ease-in;
	transition: all 0.2s ease-in;
	
}

#navigationMenu .menu{
	background:url(afb/menu-tooltip.png) no-repeat;
	background-size:170px 95px;
	height:95px; 
	width:170px;
	display:block;
	position:relative;
}

#navigationMenu .menu:hover span{ 
	 
	padding:10px 20px 20px 20px;
	overflow:visible; 
	width:610px;
	height:55px;
}
#navigationMenu .menu:hover{
	text-decoration:none;
}
#navigationMenu .menu span{
	background-image:url(afb/full-menu.png);
	background-repeat:no-repeat;
	background-size:610px 95px;
	color:#FFF;
}
#navigationMenu #inhoudmenu ul{
	list-style-type: none;
	
}
#navigationMenu #inhoudmenu li{
	margin: 10px 0px 10px 10px;
	display: inline;

}
</style>
<?php
	if ($_SESSION['paswoord'] == md5("gonzo")) 			{ 
		echo "<link href=\"http://fonts.googleapis.com/css?family=Droid+Serif:400italic\" rel=\"stylesheet\" type=\"text/css\">
			<link rel=\"stylesheet\" href=\"admin/styles.css\" type=\"text/css\" />";
	}else{
		echo "";
	}
?>

<!-- http://www.ckeditor.com/ -->
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>

<!-- http://alexgorbatchev.com/SyntaxHighlighter/
<script src="shCore.js" type="text/javascript"></script>
<script src="shBrushPhp.js" type="text/javascript"></script>
<link rel="stylesheet" href="shCore.css" type="text/css" />
<link rel="stylesheet" href="shThemeDefault.css" type="text/css" />
 -->
<?php
if ($_GET['p'] == "event-form"){
	echo "<!-- http://www.mattkruse.com/javascript/calendarpopup/ -->
		<script type=\"text/javascript\" src=\"CalendarPopup.js\"></script>
		<script type=\"text/javascript\" src=\"calendar-settings.js\"></script>";
}elseif ($_GET['p'] == "admin" && $_GET['submenu'] == "aanvragen" && $_GET['actie'] == "toevoegen"){
	echo "<!-- http://www.mattkruse.com/javascript/calendarpopup/ -->
		<script type=\"text/javascript\" src=\"CalendarPopup.js\"></script>
		<script type=\"text/javascript\" src=\"calendar-settings.js\"></script>";
}else{
	echo "";
}
?>
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_NL/all.js#xfbml=1&appId=362427607119954";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php
if ($_SESSION['paswoord'] == md5("gonzo")) {
	include_once("admin/menu.php");
}
?>
<div id="boxhouder">
<header>
  <a href="?p=home"><img src="afb/logo2.png" border="0" width="205" height="123" alt="Muziekcafé Gonzo" /></a>
</header>
    <div class="leftbox">
    <?php include_once('openingsuren.php'); ?>
    </div>
    <div class="leftbox">
    <?php include_once('agenda.php'); ?>
    </div>
    <div class="leftbox">
    <?php include_once('aanvragen.php'); ?>
    </div>
    <div class="leftbox"> 
    <div class="fb-like" data-href="http://gip.fabricedesmet.be/" data-send="true" data-width="450" data-show-faces="false" data-font="verdana"></div>
    </div>
</div>

<section>
<div id="banner">&nbsp;<!-- DIV wordt enkel gebruikt voor witruimte. -->
</div>

<nav>
	<ul id="navigationMenu">
		<li>
		  <div class="menu">
			  <span>
				<ul id="inhoudmenu">
					<li><a href="?p=home">Home</a></li>
					<li><a href="?p=fotos&submenu=optredens">Foto's</a></li>
					<li><a href="?p=drank">Drankkaart</a></li>
					<li><a href="?p=route">Route</a></li>
					<li><a href="?p=contact">Contact</a></li>
				</ul> 
			  </span>
		  </div>
		</li>
	</ul>
</nav>
	
<div id="tekst">
  <?php
  if 		($_GET ['p'] == "login"){ 
			if ($_GET ['submenu'] == "verwerklogin") 		{			 include_once ("verwerklogin.php");}
			else											{			 include_once ("login.php");}
}
if 		($_GET ['p'] == "contact"){ 
			if ($_GET ['submenu'] == "verwerkcontact") 		{			 include_once ("verwerkcontact.php");}
			else											{			 include_once ("contact.php");}
}

// elseif ($_GET ['p'] == "link") 								{		 include_once ("link");}  <---- Voorbeeld!
elseif ($_GET ['p'] == "route")								{			 include_once ("route.php");}
elseif ($_GET ['p'] == "events")							{			 include_once ("aanvragen.php");}
elseif ($_GET ['p'] == "home")								{			 include_once ("home.php");}
elseif ($_GET ['p'] == "fotos")								{			 include_once ("fotos.php");}
elseif ($_GET ['p'] == "drank")								{			 include_once ("drank.php");}
elseif ($_GET ['p'] == "agenda")							{			 include_once ("agenda.php");}
elseif ($_GET ['p'] == "openingsuren")						{			 include_once ("openingsuren.php");}
elseif ($_GET ['p'] == "event-form")						{			 include_once ("event-form.php");}
elseif ($_GET ['p'] == "admin"){ 
		if ($_SESSION['paswoord'] == md5("gonzo")) 			{ 
		
				if ($_GET ['submenu'] == "logout")				{			 include_once ("admin/logout.php");}
				if ($_GET ['submenu'] == "foto"){
					
					if ($_GET ['actie'] == "toevoegen") 		{		 include_once ("admin/foto/toevoegen.php");	}
					elseif ($_GET ['actie'] == "aanpassen") 	{		 include_once ("admin/foto/aanpassen.php");	}
					elseif ($_GET ['actie'] == "verwijderen") 	{		 include_once ("admin/foto/verwijderen.php");	}
					else  										{		 include_once ("foto.php");}
					
				}elseif ($_GET ['submenu'] == "agenda"){
					
					if ($_GET ['actie'] == "toevoegen") 		{		 include_once ("admin/agenda/toevoegen.php");	}
					elseif ($_GET ['actie'] == "aanpassen") 	{		 include_once ("admin/agenda/aanpassen.php");	}
					elseif ($_GET ['actie'] == "verwijderen") 	{		 include_once ("admin/agenda/verwijderen.php");	}
					else  										{	     include_once ("agenda.php");}
					
				}elseif ($_GET ['submenu'] == "drank") 	{
					
					if ($_GET ['actie'] == "toevoegen") 		{		 include_once ("admin/drank/toevoegen.php");	}
					elseif ($_GET ['actie'] == "aanpassen") 	{		 include_once ("admin/drank/aanpassen.php");	}
					elseif ($_GET ['actie'] == "verwijderen") 	{		 include_once ("admin/drank/verwijderen.php");	}
					else  										{	     include_once ("drank.php");}
					
				}elseif ($_GET ['submenu'] == "aanvragen") 	{
					
					if ($_GET ['actie'] == "beheer") 			{		 include_once ("admin/aanvragen/beheer.php");		}
					elseif ($_GET ['actie'] == "toevoegen") 	{		 include_once ("admin/aanvragen/toevoegen.php");	}
					elseif ($_GET ['actie'] == "delete") 		{		 include_once ("admin/aanvragen/delete.php");		}
					else  										{		 include_once ("aanvragen.php");} 
					
				}elseif ($_GET ['submenu'] == "homepage") 	{
					
					if ($_GET ['actie'] == "aanpassen") 		{	 	 include_once ("admin/homepage/aanpassen.php");	}
					
					else  										{		 include_once ("home.php");}
				}else 										{			include_once ("admin/index.php");}
		
		
		
		}else { include_once ("login.php");}
		
}else{
	include_once('home.php');
	
}
  
?>  
</div>
</section>
</body>
</html>